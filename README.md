# ESC6

#### 项目介绍
腾讯课堂：笔记
郑潇冰的ECMAScript 6 快速入门
https://ke.qq.com/webcourse/index.html#cid=181775&term_id=100214873&taid=1062046628234767

#### 软件架构
软件架构说明


#### 1-let 定义变量+例子（点击按钮，出现对应点击序号）

新增的功能：
        let a =12;
        代码块：{ }包起来的代码，形成一个块作用域
                    比如：if for while
        
        特点：只能在代码块使用
        var 只有函数作用域
        

特点：
    a：let具备块级作用域
    b：不允许重复声明
            let a=12;
            let a=5;   //错的
    总之：其实let才是接近其他语言的变量

注意，let只要是花括号{ }包起来的东西，外面都不能访问了

用处：
        封闭空间：
       （function(){
                    var a=12;
        }）()
        现在：
        {let a=12}

应用：封闭空间可以解决i的问题
例子如下：点击按钮，出现对应点击序号
<script>
window.onload = function(){
    var aBtn = document.getElementsByTagName('input');
        for(var i=0;i<aBtn.length;i++){
            aBtn[i].onclick = function(){
            alert(i)
            }
        }
    }
</script>
<input type="button" value="aaa">
<input type="button" value="bbb">
<input type="button" value="ccc">

以上是不对的！点完之后都为3

引出封闭空间

现在的话：直接使用let，确定作用域块了
for(let i=0;i<aBtn.length;i++){
    aBtn[i].onclick = function(){
        alert(i)
    }
}

#### 2-const 定义常量

const---用来定义常量，一旦赋值，以后再也修改不了了
                const a = 12;
                a = 15 //报错
                
                注意：const必须给初始值，不能重复声明
                因为以后再也没法赋值了，所以声明的时候一定得有值

                用途：为了防止意外修改变量
                比如引入库名，组件名

#### 3-连接字符串（返单引号）
 
以前：‘abc’+变量名+‘ef’
        var a = 'xxx'
        var str = 'yyy'+a+'yyyy'
        alert(str)

现在：`abc${变量名}ef`：用上${a}和返单引号`(shift键切换+左上角~)
        var a = 'xxx'
        var str = `yy${a}yy`
        alert(str)

接下来：解构赋值


#### 解构赋值：左边右边长得一样
<script>
var [a,b,c] = [1,2,3]
console.log(a,b,c)

和json配合：和顺序无关
var {aa,bb,cc} = {aa:11,cc:22,bb:33}
console.log(aa,bb,cc) //11 33 22 和顺序无关
</script>

模式匹配：--左侧的样子，需要和右侧一样

        var [a,[b,c],d] = [1,[2,3],4]      console.log(a,b,c,d)
        var [{a,e},[b,c],d] = [{e:'eee',a:'aaa'},[3,4],5]    console.log(a,b,c,d,e)

交互：数据解析
        [{title:'',href:'',img:''}]

应用：交互
例子一：
<script>
var json={
    'mukewang':[
        {
            'name':'hhh',
            'password':'h1h2h3'
        }
    ],
    'wangyiyun':[
       {
            'name':'www',
            'pas':'w1w2w3'
        }
    ]
}
var {mukewang,wangyiyun} = json;
console.log(wangyiyun)//{name: "www", pas: "w1w2w3"}
</script>


例子二：
var arr = [{title:"hjt",age:18,img:"img1"}]
var [{title,age,img}] = arr
console.log(title)//hjt


解构赋值-还可以给默认值
    var json={}
    var a=json.a||12

语法：
    var {time=12,id=0} = {};


应用在之前的例子——运动框架：

function move(obj,json,options){
    options = options || {};
    options.time = options.time||300;
}
function move(obj,json,{time=300}={}){
}

解构赋值-返回值
function show(){
    return {left:100,topp:200}
}
var {left,topp} = show();
console.log(left, topp) //100 200

接下来：复制数组

#### 5-复制数组--引出三个点
复制数组：
a：循环
b：Array.from(arr)
c：var arr2 = [...arr]

function show(...args){
    args.push(5);
    console.log(args)
}
show(1,2,3,4


例1：-这个直接赋值，是不行的
var arr1 = [1,2,3]
var arr2 = arr1
arr2.pop()
console.log(arr1,arr2);

这个方法不行，一旦arr2改变，arr1的数就会发生变化。

例2：-可以用for遍历复制：
var arr1 = [1,2,3];
var arr2 = [];
for(var i=0;i<arr1.length;i++){
    arr2[i] = arr1[i];
}
arr2.pop();
console.log(arr2,arr1)

例:3：-Array.from( )
console.log("<!-----复制数组3---->")
var arr3 = [1,2,3,4,5,6,7];
var arr4 = Array.from(arr3);
arr4.pop();
console.log(arr3,arr4)

例4：-[...arr] ------三个点（超引用）
var arr5 = [1,2,3,4];
var arr6 = [...arr5];
arr6.pop();
console.log(arr5,arr6)

三个点的用法：
可以使argument对象，在用的时候变成数组

错误用法：
// function add(){
// console.log(argument)//会提示argument is not defined
// }
// add(1,2,3)

正确用法：
function add2(...args){
    args.push(5)
    console.log(args)
}
add2(1,2,3)

接下来：循环

#### 6-循环-for...of--map对象
循环：
            普通for
            for in
            for of 循环，可以循环数组，但是不能循环json,真正目的是为了循环 map对象（下面见）。

遍历（迭代、循环）整个对象，表现类似for in

var arr = ['apple','banana','pear','orange']
for(var i=0;i<arr.length;i++){
    console.log(i);//索引
}
for(var i in arr){
    console.log(i);//索引
}
for(var i of arr){
    console.log(i);//得到的是值
}

注意：for..of也可以循环数组
for(var i of arr.entries()){}//索引、值都循环
for(var i of arr.keys()){}//只循环索引
for(var i of arr.values()){}//只循环值，或者：
for(var i of arr){}



但是for of不仅仅用在遍历上，还用在json中，但不能循环json

Map对象(配合for of循环)：
    和json相似，也是一种key-value形式
    Map对象为了和for of循环配合而生的
定义   var map = new Map();
设置：map.set('a','apple');
获取：map.get('a')
删除：map.delete('a')

Map对象举例：
var map = new Map();
map.set('a','apple');
map.set('b','apple2');
console.log(map)
// 获取
alert(map.get('a'))
map.delete('b')//只剩下a了

Map+（for of）循环例子：(换成for in的话，没有效果)
var map2 = new Map()
map.set('a','apple');
map.set('b','apple2');

//第一种方法：
for(var name of map){
    console.log(name)//有名字有值a,apple  b,apple2。。。
}
//第二种方法：
for(var [key,value] of map){
    console.log(key,value)//a apple  b apple2
}
注意：var name of map的map实质上是var name of map.entries()

// 实质的map为：map.entries())
for(var name of map.entries()){
    console.log(name)
}

//部分循环
for(var key of map.keys()){//只是循环key
    console.log(key)
}
for(var value of map.values()){ //只是循环value
    console.log(value )
}

下面就是箭头函数，继承与面向对象了！